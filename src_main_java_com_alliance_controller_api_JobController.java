package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Job;
import com.alliance.service.JobService;
import com.alliance.utility.DataPackager;

@RestController( value="jobApiController" )
@RequestMapping( value="/api/job" )
public class JobController {

	@Autowired
	public JobService jobService;
	
	@RequestMapping( method=RequestMethod.POST, value="/getJobById")
	public Object getJobById(Job job){
		DataPackager datapackager = new DataPackager();
		
		int jobId = job.getJobId();
		
		job = jobService.getJobById(jobId);
		datapackager.setDataPackager(job);
		
		return datapackager;
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/getAllJob")
	public List<Job> getAllJob(Job job){
		List<Job> listJobs;
		
		listJobs = jobService.getAllJob();	
		return listJobs;
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/getJobBySearch")
	public List<Job> getJobBySearch(Job job){
		List<Job> listJobs;
		String jobName = job.getJobName();
		
		listJobs = jobService.getJobBySearch(jobName);	
		return listJobs;
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/addJob")
	public void addJob(Job job) {
		
		String jobName = job.getJobName();
		String jobDesc = job.getJobDesc();
		String jobBuhead = job.getBUhead();
		String jobBow = job.getBOW();
		String jobDate = job.getJobPosted();
		int jobAdmin = job.getJobAdmin();
		
		job.setJobName(jobName);
		job.setJobDesc(jobDesc);
		job.setBUhead(jobBuhead);
		job.setBOW(jobBow);
		job.setJobPosted(jobDate);
		job.setJobAdmin(jobAdmin);
		
		jobService.addJob(job);
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/updateJob")
	public void updatejob(Job job) {
		
		String jobName = job.getJobName();
		String jobDesc = job.getJobDesc();
		String jobBuhead = job.getBUhead();
		String jobBow = job.getBOW();
		int jobId = job.getJobId();
		
		job.setJobName(jobName);
		job.setJobDesc(jobDesc);
		job.setBUhead(jobBuhead);
		job.setBOW(jobBow);
		job.setJobId(jobId);
		
		jobService.updateJob(job);
	}
	
}
