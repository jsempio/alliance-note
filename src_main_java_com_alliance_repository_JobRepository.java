package com.alliance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alliance.entity.Job;

public interface JobRepository extends JpaRepository<Job, String> {
	
	@Query("select j from #{#entityName} j where j.jobId = ?1")
	Job getJobById(int jobId);
	
	@Query("select j from Job j")
	List<Job> getAllJob();
	
	@Modifying
	@Query(value = "INSERT INTO job (job_name,job_desc,job_views,job_posted,job_status,job_buhead,job_bow,admin_id) VALUES (:job,:desc,:views,:posted,:status,:buhead,:bow,:jobAdmin)", nativeQuery = true)
	void addJob(@Param("job") String jobName, @Param("desc") String jobDesc, @Param("views") int jobViews, 
			      @Param("posted") String jobPosted, @Param("status") boolean jobStatus, @Param("buhead") String buhead, @Param("bow") String bow, @Param("jobAdmin") int jobAdmin);
	
	@Query("select j from Job j where j.jobName like %:jobname%")
	List<Job> findJobBySearch(@Param("jobname") String jobname);
	
	@Modifying
	@Query(value = "UPDATE job SET job_name = :job, job_desc = :desc, job_buhead = :buhead, job_bow = :bow WHERE job_id = :jobid", nativeQuery = true)
	void updateJob(@Param("job") String jobName, @Param("desc") String jobDesc, @Param("buhead") String buhead, @Param("bow") String bow, @Param("jobid") int id);
}
