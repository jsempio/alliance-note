package com.alliance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "job", schema = "jumpstart2019", catalog = "")
public class Job {
	
	int jobId, jobViews, jobAdmin;
	boolean jobStatus;
	String jobName, jobDesc, jobPosted;
	String BUhead, BOW;
	
	@Id @GeneratedValue
	@Column( name = "job_id", nullable = false, length = 11)
	public int getJobId() {
		return jobId;
	}
	
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	
	@Column( name = "admin_id", nullable = false, length = 11)
	public int getJobAdmin() {
		return jobAdmin;
	}

	public void setJobAdmin(int jobAdmin) {
		this.jobAdmin = jobAdmin;
	}

	@Column( name = "job_views", nullable = true, length = 11)
	public int getJobViews() {
		return jobViews;
	}
	
	public void setJobViews(int jobViews) {
		this.jobViews = jobViews;
	}
	
	@Column( name = "job_status", nullable = true, length = 1)
	public boolean isJobStatus() {
		return jobStatus;
	}
	
	public void setJobStatus(boolean jobStatus) {
		this.jobStatus = jobStatus;
	}
	
	@Column( name = "job_name", nullable = true, length = 30)
	public String getJobName() {
		return jobName;
	}
	
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	@Column( name = "job_desc", nullable = true, length = 1024)
	public String getJobDesc() {
		return jobDesc;
	}
	
	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}
	
	@Column( name = "job_posted", nullable = true, length = 50)
	public String getJobPosted() {
		return jobPosted;
	}
	
	public void setJobPosted(String jobPosted) {
		this.jobPosted = jobPosted;
	}
	
	@Column( name = "job_buhead", nullable = true, length = 50)
	public String getBUhead() {
		return BUhead;
	}
	
	public void setBUhead(String bUhead) {
		BUhead = bUhead;
	}
	
	@Column( name = "job_bow", nullable = true, length = 50)
	public String getBOW() {
		return BOW;
	}
	
	public void setBOW(String bOW) {
		BOW = bOW;
	}
	
}
