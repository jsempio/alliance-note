<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import = "java.io.*,java.util.*" %>
<%

	String displayDB = "";
	String displayJO = "";
	String displayRB = "";
	String displayRP = "";
	String displayL = "";

	String adminid = request.getParameter("adminid");
	
	if(session.getAttribute("adminId") != null){
		;
	}
	else if(adminid != null){
		if(session.getAttribute("adminId") == null){
			session.setAttribute("adminId",adminid);
		}
	} else if(session.getAttribute("applicantId") == null){
		response.sendRedirect("localhost:8010/jumpstart2019/login");
	}

   //Log out
   String logout = String.valueOf(request.getParameter("logout"));
   if(logout.equals("logout")){
	   session.invalidate();
	   System.out.println("Log-out");
	   response.sendRedirect("localhost:8010/jumpstart2019/login");
	   return;
   }
   
%>
<!DOCTYPE html>
<html>
    <head>
    
    <title>Recruitment Dashboard</title>
    
    <c:url value="/js/bootstrap.js" var="bootstrapJs" />
    <c:url value="/js/main.js" var="mainJs" />
    <c:url value="https://code.jquery.com/jquery-3.3.1.js" var="jqueryJs" />
    <c:url value="/js/popper.min.js" var="popperJs" />
        
    <c:url value="/css/custom.css" var="customCss" />
    <c:url value="/css/bootstrap-grid.css" var="bootstrapGrid" />
	<c:url value="/css/bootstrap-reboot.css" var="bootstrapReboot" />
	<c:url value="/css/bootstrap.css" var="bootstrapMain" />
	<c:url value="https://use.fontawesome.com/releases/v5.6.3/css/all.css" var="fontawesome" />
	
	<link href="${customCss}" rel="stylesheet" />
	<link href="${bootstrapGrid}" rel="stylesheet" />
	<link href="${bootstrapReboot}" rel="stylesheet" />
	<link href="${bootstrapMain}" rel="stylesheet" />
	<link href="${fontawesome}" rel="stylesheet" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"/>
    
    </head>

    <body>
        <%@include file="header.jsp" %>

        
        <div class="container-fluid body-content">
            <!-- JOB OPENINGS -->
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 search">
                            <button id="searchBtn" class="btn colored-button small-button form-control">Search</button>
                            <input id="search" type="text" placeholder="Search for Job (ex. title, postion)" class="form-control" style="margin-right: 1%;">
                        </div>
                    </div>
                    <br>
                    <div class="section">
                        <div class="section-header">
                            <h5>Open Job Positions
                                <!-- Open Position Button is only for admin -->
                                <button class="btn small-button white-button float-button" style="margin-top: -6px;" onclick="document.getElementById('createJob').style.display='block'">Open A Position</button>
                            </h5>
                        </div>
                        <div id="jobHTML" class="section-body padding-top-bottom-zero">
                            <p class="job-tile">
                                <a href="${pageContext.request.contextPath}/jumpstart2019/jobPage">Job Name</a>
                                <br>
                                <span class="fa fa-eye fa-sm"></span>&nbsp;<i>170 Views</i>
                                <br>
                                Velit velit aliquip est qui in ullamco sit do irure. Laborum deserunt Lorem sint esse magna ut est ut non cupidatat. Ea culpa aliqua laboris ullamco et adipisicing velit elit proident officia nostrud irure. Tempor tempor quis aute fugiat aliqua dolore nisi duis excepteur non. Commodo deserunt velit non ex duis magna ullamco id aliqua proident culpa labore adipisicing. Esse dolor minim ipsum consequat aute laborum.
                            </p>
                            <p class="job-tile">
                                <b>Job Name</b>
                                <br>
                                <span class="fa fa-eye fa-sm"></span>&nbsp;<i>170 Views</i>
                                <br>
                                In nostrud est sunt incididunt ea enim laborum aliqua id. Fugiat exercitation elit et aliqua aliquip dolore esse amet voluptate esse labore sunt nulla. Enim mollit non excepteur nulla et reprehenderit aute proident nostrud ad officia amet duis. Reprehenderit adipisicing ut irure ipsum incididunt mollit proident tempor labore. Fugiat ad non irure culpa tempor non velit anim deserunt sit.
                            </p>
                            <p class="job-tile">
                                <b>Job Name</b>
                                <br>
                                <span class="fa fa-eye fa-sm"></span>&nbsp;<i>170 Views</i>
                                <br>
                                Id ea eiusmod velit qui cupidatat Lorem incididunt ut ad eu eiusmod adipisicing magna. Id sint fugiat dolor occaecat voluptate occaecat. Nisi est eu irure sint et enim sint esse.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of job openings -->



            <!-- MODALS -->
            <div class="modal bd-example-modal-lg" id="createJob" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="createJob">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>Create Candidate Profile</h5>
                        </div>
                        <div class="modal-body section-body">
                               <div class="row">
                                     <div class="col-md-12">
                                        <p>Job Name<input id="job_name" class="form-control"></p>
                                        <p>Business Unit Head<input id="job_buhead" class="form-control"></p>
                                        <p>BOW<input id="job_bow" class="form-control"></p>
                                        <p>Job Description<textarea id="job_desc" class="form-control"></textarea></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn bordered-button full-width-button small-button" type="button" onclick="document.getElementById('createJob').style.display='none'">Cancel</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button name="create" id="createBtn" class="btn full-width-button colored-button small-button">Create</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <script type="text/javascript" src="${jqueryJs}"></script>
		<script type="text/javascript" src="${popperJs}"></script>
		<script type="text/javascript" src="${bootstrapJs}"></script>
		<script type="text/javascript" src="${mainJs}"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			
			var applicant = "${sessionScope.applicantId}";
			console.log(applicant);
			var jobListHTML = $("#jobHTML"); 
			var jobHTML = "";
			var search = $("#search").val();
			
			jobListHTML.html("");
			
			//Get All Job
			$.ajax({
					type: "post",
					url: "http://localhost:8010/api/job/getAllJob",
					dataType: "json",
					data:{
						jobName: ""
					},
					success: function(DataPackager){
						var data = DataPackager;
						var jobHREF = "${pageContext.request.contextPath}/jumpstart2019/jobPage";
						console.log("JOBS RETRIEVED SUCCESSFULLY");
						
						$.each(data, function (index, value) {
							console.log(value.jobName);
							jobHREF = "${pageContext.request.contextPath}/jumpstart2019/jobPage?jobId=" + value.jobId;
							jobEDIT = "${pageContext.request.contextPath}/jumpstart2019/jobEdit?jobId=" + value.jobId;
							
							jobHTML += "<p class='job-tile'>";
							jobHTML += "<a href='" + jobHREF +"'>" + value.jobName + "</a>";
			                jobHTML += "<br>";         
			             	jobHTML += "<span class='fa fa-eye fa-sm'></span>&nbsp;<i>" + value.jobViews + " Views</i>";
			             	jobHTML += "<a class='job-btn edit' href=" + jobEDIT + ">Edit</a>";
			             	jobHTML += "<a class='cursor job-btn delete'><i class='fas fa-trash'></i></a>";
			                jobHTML += "<br>";  
			                jobHTML += value.jobDesc;         
			                jobHTML += "</p>";            
					    });
						
						jobListHTML.html(jobHTML);
					}
				});
			
			$("#searchBtn").click(function(){
				jobListHTML.html("");
				jobHTML = "";
				search = $("#search").val();
				
				//Get All Job
				$.ajax({
						type: "post",
						url: "http://localhost:8010/api/job/getJobBySearch",
						dataType: "json",
						data:{
							jobName: search
						},
						success: function(DataPackager){
							var data = DataPackager;
							var jobHREF = "${pageContext.request.contextPath}/jumpstart2019/jobPage";
							console.log("JOBS RETRIEVED SUCCESSFULLY");
							
								$.each(data, function (index, value) {
									console.log(value.jobName);
									jobHREF = "${pageContext.request.contextPath}/jumpstart2019/jobPage?jobId=" + value.jobId;
									
									jobHTML += "<p class='job-tile'>";
									jobHTML += "<a href='" + jobHREF +"'>" + value.jobName + "</a>";
					                jobHTML += "<br>";         
					             	jobHTML += "<span class='fa fa-eye fa-sm'></span>&nbsp;<i>" + value.jobViews + " Views</i>";
					                jobHTML += "<br>";  
					                jobHTML += value.jobDesc;         
					                jobHTML += "</p>";
							    });
							
							
							jobListHTML.html(jobHTML);
						}
				});
			});
			
			$("#createBtn").click(function(){
				var buhead = $("#job_buhead").val();
				var name = $("#job_name").val();
				var desc = $("#job_desc").val();
				var bow = $("#job_bow").val();
				
				var d = new Date();
				var month = d.getMonth()+1;
				var day = d.getDate();
				var date = d.getFullYear() + '/' +
				    ((''+month).length<2 ? '0' : '') + month + '/' +
				    ((''+day).length<2 ? '0' : '') + day;
				
				 $.ajax({
					type: "post",
					url: "http://localhost:8010/api/job/addJob",
					dataType: "json",
					data:{
						jobName: name,
						jobDesc: desc,
						BUhead: buhead,
						BOW: bow,
						jobPosted: date,
						jobAdmin: 1
					},
					success: function(DataPackager){
						console.log("JOB CREATED");
					}
				});
				
			});
			
		});
		</script>
        
    </body>
</html>