package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Job;
import com.alliance.repository.JobRepository;

@Service("jobService")
@Transactional( rollbackFor = Exception.class)
public class JobService {

	@Autowired
	public JobRepository jobRepository; 
	
	public Job getJobById(int jobId){
		return jobRepository.getJobById(jobId);
	}
	
	public List<Job> getAllJob(){
		return jobRepository.getAllJob();
	}
	
	public List<Job> getJobBySearch(String jobName){
		return jobRepository.findJobBySearch(jobName);
	}
	
	public void addJob(Job job) {
		String jobName = job.getJobName();
		String jobDesc = job.getJobDesc();
		int jobViews = job.getJobViews();
		String jobPosted = job.getJobPosted();
		boolean jobStatus = job.isJobStatus();
		String buhead = job.getBUhead();
		String bow = job.getBOW();
		int jobAdmin = job.getJobAdmin();
		
		jobRepository.addJob(jobName, jobDesc, jobViews, jobPosted, jobStatus, buhead, bow, jobAdmin);
	}
	public void updateJob(Job job) {
		String jobName = job.getJobName();
		String jobDesc = job.getJobDesc();
		String buhead = job.getBUhead();
		String bow = job.getBOW();
		int id = job.getJobId();
		
		jobRepository.updateJob(jobName, jobDesc, buhead, bow, id);
	}
}
