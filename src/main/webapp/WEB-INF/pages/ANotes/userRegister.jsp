<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
   <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<spring:url value="/js/main.js" var="customJS" />
<spring:url value="/css/main.css" var="customCSS" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Insert title here</title>
<link href="${customCSS}" rel="stylesheet">

</head>
<body>

  <form method="post" action="http://localhost:8080/irtmhr/adminlogin">
	<div class='wrapper'> 
           <div id='bgimage' class='bgimage'> 
              <div class='mainbox'>
          
               <div class='loginbox'> 
                   <p class='logintitle'> ANOTES  </p>

                   <div class='divput'> 
				   <p class='regLabel'> Username </p>
                   <input type='email' placeholder ='username' class='loginputs' id='userRegister' required='required' name="username"> 
                   </div>

                    <span id='loginusermsg'>  </span>

                    <div  class='divput'> 
					<p class='regLabel'> Password </p>
                   <input type='password' placeholder ='password' class='loginputs' id='passRegister' required='required' name="password">  
                   </div> 
				   
				   <div  class='divput'> 
					<p class='regLabel'> Password </p>
                   <input type='password' placeholder ='password' class='loginputs' id='passRegisterVerify' required='required' name="passwordVerify">  
                   </div>

                   <span id='loginpassmsg'> </span>
                  <p style='text-align: center;'>
                    <input type='submit' name="register" class='logbtn' value='login' id='registerUserBtn'> 
                   </p>
<!--                <p style='text-align:center'> No account yet? <span id='link' class='signup'> Sign up </span> here</p>-->
               </div>
              </div>  
           </div>
       </div>
     </form>

</body>
</html>