package com.alliance.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fakenote", schema = "springboot")
public class FakeNote implements Serializable{
	private int noteId;
	private String msg;
	private Category category;
	
	@Id
	@GeneratedValue
	@Column(name = "noteId", nullable = false)
	public int getNoteId() {
		return noteId;
	}
	public void setNoteId(int noteId) {
		this.noteId = noteId;
	}
	
	@Column(name = "msg", nullable = false, length = 100)
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "catId")
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
}