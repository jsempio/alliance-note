package com.alliance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user", schema = "springboot", catalog = "")
public class User {
	
	private int id;
	private String user;
	private String pass;
	

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "username", nullable = false, length = 50)
	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}

	@Column(name = "userpass", nullable = false, length = 20)
	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
	
	
}
