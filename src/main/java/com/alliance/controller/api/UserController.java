package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.User;
import com.alliance.service.UserService;

@RestController( value="userApiController" )
@RequestMapping( value="/api/user" )
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping( method=RequestMethod.GET, value="/getAllUser")
	public List<User> getAllUser(User user){
		List<User> listUsers;
		
		listUsers = userService.getUserList();
		return listUsers;
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/updateUser")
	public void updateUser(User user){
		//String userName  = user.getUser();
		//String userPass = user.getPass();
		//int userId = user.getId();
		
		String userName  = "Kentoy";
		String userPass = "baboy";
		int userId = 2;
		
		//String userName = request.getParameter("user");
		//String userPass = request.getParameter("pass");
		//int userId = request.getParameter("id");
		user.setId(userId);
		user.setUser(userName);
		user.setPass(userPass);
		
		userService.updateUser(user);
	}
	
	
	@RequestMapping( method=RequestMethod.POST, value="/addUser")
	public void addUser(User user) {
		
		String userName  = user.getUser();
		String userPass = user.getPass();
		
	
		user.setUser(userName);
		user.setPass(userPass);
		
		userService.addUser(user);
		
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/deleteUser")
	public void deleteUser (User user) {
		
		int id = 3;
	
		user.setId(id);
		
		userService.deleteUser(user);
	}
	
}
