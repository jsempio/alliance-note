package com.alliance.controller.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.service.CategoryService;
import com.alliance.entity.Category;

@RestController(value = "categoryApiController")
@RequestMapping(value = "/api/category")
public class CategoryController {
	
	final private String CATNAME_PARAM = "categoryName";
	final private String CATID_PARAM = "categoryID";
	
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public ResponseEntity<List<Category>> getAllCategoryList(){
		return ResponseEntity.ok(categoryService.readAll());
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public ResponseEntity<Boolean> addCategory(HttpServletRequest req){
		if(null != req.getParameter(CATNAME_PARAM)) {
			categoryService.add(req.getParameterValues(CATNAME_PARAM)[0]);
		}
		return ResponseEntity.ok(true);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/update")
	public ResponseEntity<Boolean> updateCategory(HttpServletRequest req){
		if(null != req.getParameter(CATID_PARAM)) {
			if(null != req.getParameter(CATNAME_PARAM)) {
				categoryService.update(
						req.getParameterValues(CATID_PARAM)[0],
						req.getParameterValues(CATNAME_PARAM)[0]
				);
			}
		}
		return ResponseEntity.ok(true);
	}
}