package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.FakeNote;
import com.alliance.service.FakeNoteService;

@RestController(value = "fakeNoteApiController")
@RequestMapping(value = "/api/fakenote")
public class FakeNoteController {
	
	@Autowired
	private FakeNoteService fakeNoteService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public ResponseEntity<List<FakeNote>> getAllListByCat(){
		return ResponseEntity.ok(fakeNoteService.getAll());
	}
}
