package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.User;
import com.alliance.repository.UserRepository;

@Service("userService")
@Transactional(rollbackFor = Exception.class)
public class UserService {
	
	    @Autowired
	    public UserRepository UserRepository; 


		public List<User> getUserList() {
			return UserRepository.listAll();
		}
		
		public void updateUser(User user) {
			
			String userName = user.getUser();
			String userPass = user.getPass();
			int userId = user.getId();
			
			UserRepository.updateUser(userName, userPass, userId);
		}
		
		public void addUser (User user) {
			String userName = user.getUser();
			String userPass = user.getPass();
			
			UserRepository.addUser(userName,userPass);
			
		}
		
		public void deleteUser (User user) {
			
			int id = user.getId();
			
			UserRepository.deleteUser(id);
		}
}
