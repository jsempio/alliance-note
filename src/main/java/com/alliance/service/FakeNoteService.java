package com.alliance.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.entity.FakeNote;
import com.alliance.repository.FakeNoteRepository;

@Service("fakeNoteService")
@Transactional( rollbackOn = Exception.class)
public class FakeNoteService {
	
	@Autowired
	private FakeNoteRepository fakeNoteRepository;
	
	public List<FakeNote> getAll(){
		return fakeNoteRepository.findAll();
	}
}
