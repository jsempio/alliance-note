package com.alliance.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.entity.Category;
import com.alliance.repository.CategoryRepository;

@Service("categoryService")
@Transactional( rollbackOn = Exception.class)
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	public List<Category> readAll(){
		return categoryRepository.listAllCategory();
	}
	
	public void add(String name) {
		categoryRepository.addCategory(name);
	}
	
	public void update(String id, String name) {
		categoryRepository.updateCategory(id, name);
	}
}
