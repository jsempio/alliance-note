package com.alliance.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.alliance.entity.User;



public interface UserRepository extends JpaRepository<User, String>  {


	@Query("select u from #{#entityName} u")
	List<User> listAll();

	@Modifying
	@Query(value = "UPDATE user SET username = :uname, userpass = :upass WHERE id = :uid", nativeQuery = true)
	void updateUser(@Param("uname") String userName, @Param("upass") String userPass, @Param("uid") int userId);
	
	@Modifying
	@Query(value = "INSERT INTO user (username,userpass) VALUES (:uname,:upass)", nativeQuery = true)
	void addUser(@Param("uname") String userName, @Param("upass") String userPass);

	
	@Modifying
	@Query(value = "DELETE from user WHERE id= :uid", nativeQuery = true)
	void deleteUser(@Param("uid") int id);

}
