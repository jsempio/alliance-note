package com.alliance.repository;

import com.alliance.entity.Category;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;

@Repository("categoryRepository")
public interface CategoryRepository extends JpaRepository<Category, String> {
	
	@Query("select u from #{#entityName} u")
	List<Category> listAllCategory();
	
	@Modifying
	@Query(value = "insert into category (name) values(:name)", nativeQuery = true)
	void addCategory(@Param("name") String name);
	
	@Modifying
	@Query(value = "update category set name=:name where id=:id", nativeQuery = true)
	void updateCategory(@Param("id") String id, @Param("name") String name);
}
