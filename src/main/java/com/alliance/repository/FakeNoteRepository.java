package com.alliance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alliance.entity.FakeNote;

@Repository("fakeNoteRepository")
public interface FakeNoteRepository extends JpaRepository<FakeNote, Integer>{
}
